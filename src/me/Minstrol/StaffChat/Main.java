package me.Minstrol.StaffChat;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.messaging.PluginMessageListener;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;

public class Main extends JavaPlugin implements PluginMessageListener, Listener {

    String staffChat = "";
    private static Main instance;

    @Override
    public void onEnable() {
        instance = this;
        this.getServer().getPluginManager().registerEvents(this, this);
        this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        this.getServer().getMessenger().registerIncomingPluginChannel(this, "BungeeCord", this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;
        PermissionUser permissionUser = PermissionsEx.getUser(player);
        if (command.getName().equalsIgnoreCase("s")){
            if (permissionUser.inGroup("HELPER") || permissionUser.inGroup("MOD") || permissionUser.inGroup("ADMIN") || permissionUser.inGroup("OWNER") || permissionUser.inGroup("BUILD-TEAM")) {
                if (args.length < 1) {
                    player.sendMessage(ChatColor.GOLD + "Usage: " + ChatColor.RED + "/s [message]");
                }
                if (args.length >= 1) {
                    String msg = "";
                    for (int i = 0; i < args.length; i++) {
                        msg += args[i] + " ";
                    }
                    staffChat = ChatColor.GOLD + "[S] " + permissionUser.getPrefix() + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.AQUA + msg;
                    ByteArrayOutputStream b = new ByteArrayOutputStream();
                    DataOutputStream out = new DataOutputStream(b);
                    try {

                        out.writeUTF("Forward");
                        out.writeUTF("ALL");
                        out.writeUTF("StaffChat");
                        byte[] data = staffChat.getBytes();
                        out.writeShort(data.length);
                        out.write(data);

                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    player.sendPluginMessage(instance, "BungeeCord", b.toByteArray());

                    for (Player online : Bukkit.getOnlinePlayers()) {
                        PermissionUser permissionUser1 = PermissionsEx.getUser(online);
                        if (permissionUser1.inGroup("HELPER") || permissionUser1.inGroup("MOD") || permissionUser1.inGroup("ADMIN") || permissionUser1.inGroup("OWNER") || permissionUser1.inGroup("BUILD-TEAM")) {
                            online.sendMessage(staffChat);
                        }
                    }
                }
            } else {
                sender.sendMessage(ChatColor.RED + "You do not have permission to do this!");
            }
        }
        return false;
    }

    @Override
    public void onPluginMessageReceived(String channel, Player player, byte[] message) {
        if (!channel.equals("BungeeCord")) {
            return;
        }
        try {
            DataInputStream in = new DataInputStream(new ByteArrayInputStream(message));
            String subchannel = in.readUTF();
            if (subchannel.equals("StaffChat")) {
                short len = in.readShort();
                byte[] data = new byte[len];
                in.readFully(data);
                String s = new String(data);
                for (Player online : Bukkit.getOnlinePlayers()) {
                    PermissionUser permissionUser = PermissionsEx.getUser(online);
                    if (permissionUser.inGroup("HELPER") || permissionUser.inGroup("MOD") || permissionUser.inGroup("ADMIN") || permissionUser.inGroup("OWNER") || permissionUser.inGroup("BUILD-TEAM")) {
                        online.sendMessage(s);
                    }
                }
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
}
